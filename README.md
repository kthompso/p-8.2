# Final project #1 #
## CS 600 - P-8.2 ##
**Author**:  Kyle Thompson-Bass (kylethompsonbass@gmail.com)
## Project description##

Implement the Ford-Fulkerson algorithm with three distinct methods for detecting augmenting paths.

##Usage:##

python maxflow.py [-h] [input_file_1 ... input_file_n]

	-h : shows help

	input_file : processes max flow for the graph contained in each input file

* If no input files are specified, attempts to import the example1.txt file from the same directory *

##Input File Format##

A graph in text form.  Format is as follows:

For each line, a vertex id of the origin vertex followed by a tab and a list of semi-colon separated adjacent vertices and the capacity of the edge connecting them.  Adjacent vertices and their edge capacity are separated by a colon.  For example:

	s  {TAB} v1:3;v2:5
	v1 {TAB} v2:4;t:1
	v2 {TAB} t:3

Creates a graph with 4 vertices (s, v1, v2, t).  With 5 edges (s->v1, s->v2, v1->v2, v1->t, v2->t) which have capacities of (3, 5, 4, 1, 3) respectively.

	+-----------[5]-----------+
	|                         |
	|                         v
	s --[3]---> v1 ---[4]---> v2
			    |             |
			    |            [3]
			    |             |
			    |             v
			    +-----[1]---> t

##Augmenting Path Algorithms##

1. **DFS**: A backtracking DFT done within the original graph G which traverses edges forwards and backwards if their residual capacity is nonzero (based on direction).
2. **Reisudal DFS**: Generating residual graph R from G and doing a backtracking DFS to find a directed path from source to sink in R.
3. **Residual BFS (Edmonds/Karp)**: Generating residual graph R from G and doing a recursive BFS to find a directed path from source to sink in R.

##Description##

Runs all three maxflow finding algorithms for each graph specified in the list of input files.  If -h is specified, prints out usage information.  If no arguments are provided, runs the maxflow finding algorithms on an example graph from example1.txt (graph is from the textbook).

##Analysis##

###Time complexity of augmenting path finding algorithms:###

1.  **DFS**:
	-Does a DFT in the original network graph G, traversing forward and backward edges where the residual capacity is non zero.
	-Time complexity is O(|V| + |E|).
	-Space complexity is also O(|V|) because we maintain a list of all the visited vertices, as well as a path (which is also a list of vertices).

2.	**Residual DFS**:
	-Does a DFT in the residual graph R of G.
	-Time complexity is O(|V| + |E|).  Generating the residual graph takes Theta(|V| + |E|) time and the DFT takes O(|V| + |E|) time in the residual graph, which has O(2|E|) edges and Theta(|V|) vertices.
	-Space complexity is O(|V| + |E|).  In addition to the list of visited vertices O(|V|) and the path O(|V|), we also maintain the residual graph which has space requirements of O(|V| + |E|).

3.	**Residual BFS (Edmonds/Karp)**:
	-Generates a residual graph R of G and then runs a BFT in R to find an augmenting path.
	-Time complexity is O(|V| + |E|)
	-Space complexity is O(|V|^2)

###Time complexity of the maxflow finding algorithms (EK, FF):###

1.  Ford Fulkerson runs in O(Xf) where X is the time it takes to find an augmenting path and f is the maximum flow.  So for both the DFS and Residual DFS, it takes O((|V|+|E|)f) time to find the max flow.

2.  A breadth first search within a residual graph is equivallent to the Edmonds/Karp algorithm, as it always returns the augmenting path with least length. Edmonds/Karp runs in O(|V||E|^2).

###Example Output###

Output is a list of edges in the graph, adjusted to be a maximum flow, along with their flow and capacity.

*Output from the graph in text file example1.txt which is representative of the graph on page 384.

	Processing Graph from: example1.txt
	Finding max flow using findAugPathDFS algorithm.
	############# Flow(14) #############
	   Origin     Dest     Flow Capacity
	       v2       v3        1        3
	       v1       v3        1        1
	       v3       v4        4        5
	       v1       v4        2        2
	       v5        t        5        8
	        s       v3        5        5
	       v2       v5        5        9
	        s       v1        3        7
	        s       v2        6        6
	       v4        t        6        6
	       v3        t        3        3
	####################################
	Processing Graph from: example1.txt
	Finding max flow using findAugPathRDFS algorithm.
	############# Flow(14) #############
	   Origin     Dest     Flow Capacity
	       v2       v3        1        3
	       v1       v3        1        1
	       v3       v4        4        5
	       v1       v4        2        2
	       v5        t        5        8
	        s       v3        5        5
	       v2       v5        5        9
	        s       v1        3        7
	        s       v2        6        6
	       v4        t        6        6
	       v3        t        3        3
	####################################
	Processing Graph from: example1.txt
	Finding max flow using findAugPathRBFS algorithm.
	############# Flow(14) #############
	   Origin     Dest     Flow Capacity
	       v2       v3        1        3
	       v1       v3        1        1
	       v3       v4        4        5
	       v1       v4        2        2
	       v5        t        5        8
	        s       v3        5        5
	       v2       v5        5        9
	        s       v1        3        7
	        s       v2        6        6
	       v4        t        6        6
	       v3        t        3        3
	####################################