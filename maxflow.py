'''Final project #2 for CS 600 - P-8.2 from the book.  Implement the 
Ford-Fulkerson algorithm with three distinct methods for detecting 
augmenting paths.

Author:  Kyle Thompson-Bass - kylethompsonbass@gmail.com'''

import kgraph, sys

#TODO should NetworkGraph detect whether it's a network graph or not?

def generateResidualGraph(netgraph):
    '''Generates a graph from provided netgraph which contains all vertices
        as well as all edges with non-zero residual capacity and all backwards
        edges with non-zero residual capacity'''
    graph = kgraph.NetworkGraph()
    for v in netgraph.vertices().keys():
        graph.addVertex(v)
    for e in netgraph.edges().values():
        if e.getResidualCapacity() != 0:
            e = graph.addEdge(e.origin, e.dest, e.getCapacity(), e.getFlow())
            e.setAttr('backedge', False)
        if e.getResidualCapacity(back=True) != 0:
            e = graph.addEdge(e.dest, e.origin, e.getCapacity(), e.getFlow())
            e.setAttr('backedge', True)
    return graph

def findAugPathRDFS(netgraph, src, sink):
    ''' Creates a residual graph R and then does a DFS to find a directed
        path in R.'''
    return netgraph.vPathToAugEPath(backtrackFindPath(generateResidualGraph(netgraph), src, sink))

def findAugPathRBFS(netgraph, src, sink):
    '''Creates a residual graph R and then does a BFS to find a directed path
        in R.'''
    return netgraph.vPathToAugEPath(BFSFindPath(generateResidualGraph(netgraph), src, sink))

def backtrackFindPath(netgraph, src, sink):
    '''Finds a path in residual netgraph R from src to sink using a DFT.'''
    path = [src]
    seen = [src]
    while len(path) > 0:
        outbound = netgraph.getOutboundEdges(path[-1])
        #print('path:', list([str(e) for e in path]))
        backtrack = True
        for edge in [e for e in outbound if e.dest not in seen]:
            if edge.dest == sink: #Path discovered
                return path + [sink]
            else:
                path.append(edge.dest)
                seen.append(edge.dest)
                backtrack = False
                break
        if backtrack: #No untraversed edges were found, backtrack
            path = path[:-1] #backtrack by one
    return None

def findAugPathDFS(netgraph, src, sink):
    '''Does a depth first traversal of the graph. Goes along edges where the
    residual capacity is non-zero'''
    for e in netgraph.edges().values():
        e.setAttr('backedge', False)
    path = [src]
    seen = [src]
    while len(path) > 0:
        outbound = netgraph.getOutboundEdges(path[-1])
        inbound = netgraph.getInboundEdges(path[-1])
        backtrack = True
        for e in (inbound + outbound):
            if e.origin == path[-1] and e.dest not in seen and e.getResidualCapacity() != 0:
                e.setAttr('backedge', False)
                if e.dest == sink:
                    return netgraph.vPathToAugEPath(path + [sink])
                path.append(e.dest)
                seen.append(e.dest)
                backtrack = False
                break
            if e.dest == path[-1] and e.origin not in seen and e.getResidualCapacity(True) != 0:
                e.setAttr('backedge', True)
                path.append(e.origin)
                seen.append(e.origin)
                backtrack = False
                break
        if backtrack:
            path = path[:-1]
    return None

def BFSFindPath(netgraph, src, sink, path=[]):
    '''Does a recursive BFS on a residual graph to find an augmenting path.'''
    if len(path) > 0 and path[-1] == sink: # Path was found.
        return path
    if len(path) == 0:
        edges = netgraph.getOutboundEdges(src)
        path = [src]
    else:
        edges = netgraph.getOutboundEdges(path[-1])
    for e in edges:
        if e.dest not in path:
            p = BFSFindPath(netgraph, src, sink, path + [e.dest])
            if p != None:
                return p
    return None #No path was found from src to sink

def fordFulkersonMaxFlow(augPathFunc, netgraph, src, sink, maxAttempts=100):
    '''Sets graph to maxflow version of itself'''
    print("Finding max flow using " + augPathFunc.__name__ + " algorithm.")
    p = augPathFunc(netgraph, src, sink)
    while p and maxAttempts:
        maxAttempts -= 1
        pathResCap = sys.maxsize
        for e in p:
            eResCap = e.getResidualCapacity(e.getAttr('backedge'))
            pathResCap = min(eResCap, pathResCap)
        for e in p:
            if e.getAttr('backedge'):
                netgraph.getEdge(e.dest, e.origin, False, True).setFlow(e.getFlow() - pathResCap)
            else:
                netgraph.getEdge(e.origin, e.dest).setFlow(e.getFlow() + pathResCap)
        p = augPathFunc(netgraph, src, sink)
    return netgraph

#TODO turn into a constructor for NetworkGraph
def importGraphFromFile(filename='example1.txt'):
    '''Reads in graph from a file.  File format should be an adjacency list
        with a vertex name followed by a tab and then a list of adjacent vertices
        and the capacity for those edges with the vertices separated by commas
        and the capacity separated by a colon.  E.g.:
            vert1   vert2:15,vert3:7,vert8:10
            vert2   vert9:7,vert3:14
            ...etc
    '''
    graph = kgraph.NetworkGraph()
    with open(filename) as f:
        line = f.readline()
        while line:
            splitOnTab = line.split('\t')
            vertName = splitOnTab[0]
            if not graph.containsVertex(vertName):
                graph.addVertex(vertName)
            for edge in splitOnTab[1].split(','):
                splitOnSC = edge.split(':')
                destName = splitOnSC[0]
                if not graph.containsVertex(destName):
                    graph.addVertex(destName)
                capacity = splitOnSC[1]
                graph.addEdge(vertName, destName, int(capacity))            
            line = f.readline()
    return graph

def usage():
    print("Usage: maxflow INPUT_FILE_1 [INPUT_FILE_2...]")

def main(input_files = ["example1.txt"]):
    '''Default graph is from section 8.4 in the Algorithm Design textbook.
        Max flow should be 14'''
    augPathFuncs = [findAugPathDFS, findAugPathRDFS, findAugPathRBFS]
    for func in augPathFuncs:
        for input_file in input_files:
            print("Processing Graph from: {:s}".format(input_file))
            ng = importGraphFromFile(input_file)
            fordFulkersonMaxFlow(func, ng, 's', 't').printFlow()

if __name__ == '__main__':
    argv = sys.argv[1:]
    if len(argv) == 0:
        main()
    elif len(argv) == 1 and argv[0] == '-h':
        usage()
        sys.exit(2)
    else:
        main(argv)