'''
File:   kgraph.py (Kyle Graph)
Descrip:    Implementation of a network graph for use with MaxFlows
Author: Kyle Thompson-Bass (kylethompsonbass@gmail.com)
'''

class Decorator(object):
    def __init__(self):
        self.attrs = {}
    def setAttr(self, key, value):
        self.attrs[key] = value
    def unsetAttr(self, value):
        if key in self.attrs:
            del self.attrs[key]
    def getAttr(self, key):
        if key in self.attrs:
            return self.attrs[key]
        return None

class Vertex(Decorator):
    def __init__(self, vertexId):
        self.vertexId = vertexId
        super(Vertex, self).__init__()
    def __str__(self):
        return ('V[ ' + str(self.vertexId) + ', ' + str(self.attrs) + ' ]')

class Edge(Decorator):
    def __init__(self, origin, dest):
        self.origin = origin
        self.dest   = dest
        self.edgeId = (origin, dest)
        super(Edge, self).__init__()
    def incidentOn(self, vertexId):
        return (vertexId == self.origin or vertexId == self.dest)
    def getId(self):
        return self.edgeId
    def getOrigin(self):
        return self.origin
    def getDest(self):
        return self.dest
    def __str__(self):
        return 'E[ ' + str(self.edgeId) + ', ' + str(self.attrs) + ' ]'

class NGEdge(Edge):
    CAPACITY = 'capacity'
    FLOW = 'flow'
    def __init__(self, origin, dest, capacity=0, flow=0):
        super(NGEdge, self).__init__(origin, dest)
        self.setCapacity(capacity)
        self.setFlow(flow)
    def getCapacity(self):
        return self.getAttr(NGEdge.CAPACITY)
    def setCapacity(self, capacity):
        self.setAttr(NGEdge.CAPACITY, capacity)
    def getFlow(self):
        return self.getAttr(NGEdge.FLOW)
    def setFlow(self, flow):
        if flow > self.getCapacity():
            print("Error: Flow cannot exceed capacity.")
            return
        self.setAttr(NGEdge.FLOW, flow)
    def getResidualCapacity(self, back=False):
        if back:
            return self.getFlow()
        return self.getCapacity() - self.getFlow()
    def __str__(self):
        return super(NGEdge, self).__str__() + ' ' + str(self.getFlow()) + '/' + str(self.getCapacity())

class DiGraph(object):
    def __init__(self):
        self.vlist   = {}
        self.elist   = {}
    def addVertex(self, vertexId):
        vertex                  = Vertex(vertexId)
        self.vlist[vertexId] = vertex
        return vertex
    def addEdge(self, origin, dest):
        if self.containsVertex(origin) and self.containsVertex(dest):
            edge = Edge(origin, dest)
            self.elist[(origin, dest)] = edge
            return edge
    def getVertex(self, vertexId):
        if self.containsVertex(vertexId):
            return self.vlist[vertexId]
        return None
    def getEdge(self, origin, dest, aug=False, possiblyBackwards=False):
        if self.containsEdge(origin, dest):
            if (not aug) or (self.elist[(origin, dest)].getResidualCapacity() != 0):
                self.elist[(origin, dest)].setAttr('backedge', False)
                return self.elist[(origin, dest)]
        if possiblyBackwards and self.containsEdge(dest, origin):
            if (not aug) or (self.elist[(dest, origin)].getResidualCapacity(True) != 0):
                self.elist[(dest, origin)].setAttr('backedge', True)
                return self.elist[(dest, origin)]
        return None
    def vertices(self):
        return self.vlist
    def edges(self):
        return self.elist
    def removeVertex(self, vertexId):
        if self.containsVertex(vertexId):
            del self.vlist[vertexId]
            # Remove all edges incident on vertexId
            for edge in self.elist:
                if edge.incidentOn(vertexId):
                    self.removeEdge(edge.getId())
    def removeEdge(self, origin, dest):
        if self.containsEdge(origin, dest):
            del self.elist[(origin, dest)]
    def containsVertex(self, vertexId):
        return (vertexId in self.vlist)
    def containsEdge(self, origin, dest):
        return (origin, dest) in self.elist
    def getOutboundEdges(self, vertexId):
        return list([e for e in self.elist.values() if (e.origin == vertexId)])
    def getInboundEdges(self, vertexId):
        return list([e for e in self.elist.values() if (e.dest == vertexId)])
    def __str__(self):
        vs = ''
        for vertex in self.vlist.values():
            vs += "\t" + str(vertex) + "\n"
        es = ''
        for edge in self.elist.values():
            es += "\t" + str(edge) + "\n"
        return "Vertices: " + vs + "\nEdges: " + es

class NetworkGraph(DiGraph):
    def addEdge(self, origin, dest, capacity=0, flow=0):
        edge = NGEdge(origin, dest, capacity, flow)
        self.elist[(origin, dest)] = edge
        return edge
    def getFlow(self, src):
        flow = 0
        for e in self.edges().values():
            if e.origin == src:
                flow += e.getFlow()
        return flow
    def printFlow(self):
        fmtStr = ''.join(list(["{: >9}"]*4))
        print("############# Flow({:d}) #############".format(self.getFlow('s')))
        print(fmtStr.format("Origin", "Dest", "Flow","Capacity"))
        for edge in self.elist.values():
            print(fmtStr.format(str(edge.edgeId[0]), str(edge.edgeId[1]), str(edge.getFlow()), str(edge.getCapacity())))
        print("####################################")
    def vPathToAugEPath(self, vPath):
        if vPath == None:
            return None
        # Convertes a path of vertices to an augmenting path of edges
        return [self.getEdge(vPath[i], vPath[i+1], True, True) for i in range(len(vPath)-1)]